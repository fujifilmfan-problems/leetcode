#!/usr/bin/env python

from collections import deque
import pytest

from leetcode import add_two_numbers_0002


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    # def __eq__(self, other):
    #     if not isinstance(other, ListNode):
    #         # Don't attempt to compare against unrelated types.
    #         return NotImplemented
    #
    #     return self.val == other.val and self.next == other.next


def create_linked_list(val_list):
    """Create a linked list from input values.

    :param val_list: LIST; values for creating linked list
    :return: OBJ; ListNode object
    """

    node = None
    next_node = None
    for _ in val_list:
        node = ListNode(val=int(_), next=next_node)
        next_node = node

    return node


def test_addTwoNumbers_not_linked_list():
    # Arrange
    l1 = [2, 4, 3]
    l2 = [5, 6, 4]
    solution = add_two_numbers_0002.Solution()
    expected = [7, 0, 8]

    # Act
    actual = solution.addTwoNumbers_not_linked_list(l1, l2)

    # Assert
    assert actual == expected


@pytest.mark.parametrize('l1, l2, exp', [
    ([2, 4, 3], [5, 6, 4], [7, 0, 8]),
    ([1, 8], [0], [1, 8])
])
def test_addTwoNumbers(l1, l2, exp):
    # Arrange
    ll1 = create_linked_list([_ for _ in l1[::-1]])
    ll2 = create_linked_list([_ for _ in l2[::-1]])
    exp_ll = create_linked_list([_ for _ in exp[::-1]])
    expected = ''
    while exp_ll:
        expected += str(exp_ll.val)
        exp_ll = exp_ll.next
    solution = add_two_numbers_0002.Solution()

    # Act
    actual_ll = solution.addTwoNumbers(ll1, ll2)
    actual = ''
    while actual_ll:
        actual += str(actual_ll.val)
        actual_ll = actual_ll.next

    # Assert
    assert actual == expected
