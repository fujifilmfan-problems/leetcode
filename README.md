### Python 3 leetcode.com solutions

#### 1. Two Sum
[Two Sum](https://leetcode.com/problems/two-sum/)  
* `1a-two_sum.py`  
   * [Submitted 2019-02-25](https://leetcode.com/submissions/detail/210656293/)  
   * Uses itertools.combinations() to create every combination of numbers, which in the last test case is >79M
   * Fails the last test case due to "Memory Limit Exceeded"  
* `1b-two_sum.py`  
   * [Submitted 2019-02-25](https://leetcode.com/submissions/detail/210710538/)  
   * I looked at the Solution to derive this answer
   
Category     | Result     | Beats
--------     | ------     | -----
Status       | Accepted   | N/A
Runtime      | 1516 ms    | 25.90%
Memory Usage | 13.6 MB    | 48.99%

* `1c-two_sum.py`  
   * [Submitted 2019-02-25](https://leetcode.com/submissions/detail/210713199/)  
   * I looked at the discussions around Python 3 to get the idea of implementing a dictionary for this problem  
   * Uses more memory than previous solution

Category     | Result    | Beats
--------     | ------    | -----
Status       | Accepted  | N/A
Runtime      | 44 ms     | 64.89%
Memory Usage | 14.3 MB   | 5.21%
   
#### 2. Add Two Numbers
[Add Two Numbers](https://leetcode.com/problems/add-two-numbers/)
* `add_two_numbers_0002.py`  
   * My first solution (now called `addTwoNumbers_not_linked_list`) did not explicitly use a linked list.
   * This page was helpful for better understanding the idea: 
     [Linked Lists in Python: An Introduction](https://realpython.com/linked-lists-python/).
     However, I did try using `deque()` before just implementing the `ListNode` class.

Category     | Result    | Beats
--------     | ------    | -----
Status       | Accepted  | N/A
Runtime      | 60 ms     | 98.83%
Memory Usage | 14.1 MB   | 5.06%

#### 3. Longest Substring Without Repeating Characters
[Longest Substring Without Repeating Characters](https://leetcode.com/problems/longest-substring-without-repeating-characters/)
* `3-longest_sub_wo_repeats.py`

#### 4. Median of Two Sorted Arrays
[Median of Two Sorted Arrays](https://leetcode.com/problems/median-of-two-sorted-arrays/)  

* `4-median_two_sorted_arrays.py`  
  * [Submitted 2019-02-22](https://leetcode.com/submissions/detail/209875272/)
  * Memory usage needs improvement, but note that the band is narrow (~12.4 - 12.9)  

Category     | Result    | Beats
--------     | ------    | -----
Status       | Accepted  | N/A
Runtime      | 104 ms    | 54.62%
Memory Usage | 12.9 MB   | 6.24%

