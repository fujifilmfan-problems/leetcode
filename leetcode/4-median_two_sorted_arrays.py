#!/usr/bin/env python

nums1 = [1]
nums2 = [1]


class Solution:

    def findMedianSortedArrays(self, list1, list2):
        # Merge arrays into one array
        merged_array = self.merge_arrays(list1, list2)

        length = len(merged_array)
        # If the array length is even, then average the two middle numbers.
        if len(merged_array) % 2 == 0:
            first_num = merged_array[(length // 2) - 1]
            second_num = merged_array[length // 2]
            return (first_num + second_num) // 2
        # Otherwise, just return the middle number.
        else:
            return merged_array[length // 2]

    @staticmethod
    def merge_arrays(list1, list2):
        merged_array = []

        n = len(list1) + len(list2)
        i = 0
        j = 0

        for _ in range(n):
            # Add smaller of the next item in "list1" and "list2" to "merged_array"; if "j" is longer than the length of
            # "list2", then "list2" has been exhausted and the elif statement is triggered if "i" is still less than
            # the length of "list1"; if "i" is longer than "list1", then "list1" has been exhausted, and the else
            # statement is triggered.
            if i < len(list1) and j < len(list2):
                if list1[i] < list2[j]:
                    merged_array.extend([list1[i]])
                    i += 1
                else:
                    merged_array.extend([list2[j]])
                    j += 1
            elif i < len(list1):
                merged_array.extend([list1[i]])
                i += 1
            else:
                merged_array.extend([list2[j]])
                j += 1

        return merged_array


solution = Solution()
median = solution.findMedianSortedArrays(nums1, nums2)
print(median)
