#!/usr/bin/env python

from collections import deque


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


llist1 = ListNode(val=2, next=ListNode(val=4, next=ListNode(val=3, next=None)))
llist2 = ListNode(val=5, next=ListNode(val=6, next=ListNode(val=4, next=None)))


class Solution:

    @staticmethod
    def addTwoNumbers_not_linked_list(l1, l2):
        first = ''
        second = ''
        for digit in l1[::-1]:
            first += str(digit)
        for digit in l2[::-1]:
            second += str(digit)
        string_sum = str(int(first) + int(second))
        return [int(i) for i in string_sum[::-1]]

    @staticmethod
    def addTwoNumbers(l1, l2):
        first_rev = ''
        second_rev = ''

        while l1:
            first_rev += str(l1.val)
            l1 = l1.next

        while l2:
            second_rev += str(l2.val)
            l2 = l2.next

        first = first_rev[::-1]
        second = second_rev[::-1]

        node = None
        next_node = None

        for _ in str(int(first) + int(second)):
            node = ListNode(val=int(_), next=next_node)
            next_node = node

        return node


solution = Solution
print(solution.addTwoNumbers(llist1, llist2))
