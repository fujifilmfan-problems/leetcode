#!/usr/bin/env python

input_array = [3, 2, 4]
target = 6


class Solution:

    @staticmethod
    def twoSum(nums, target_sum):
        for num in nums:
            difference = target_sum - num
            # If the two values are not equal, we can be confident that two different numbers are being compared
            if difference in nums and num != difference:
                # index() is slow
                return [nums.index(num), nums.index(difference)]
            # If the two numbers are equal, check whether there are duplicates or whether the same number is being
            # compared to itself
            elif difference in nums and num == difference:
                duplicates = [i for i, num in enumerate(nums) if num == difference]
                if len(duplicates) > 1:
                    return [duplicates[0], duplicates[1]]


solution = Solution()
print(solution.twoSum(input_array, target))
