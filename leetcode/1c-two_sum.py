#!/usr/bin/env python

input_array = [3, 3]
target = 6


class Solution:

    @staticmethod
    def twoSum(nums, target_sum):
        diffs = {}
        for i, num in enumerate(nums):
            diff = target_sum - num
            # Check diffs dictionary for the difference
            if diff in diffs:
                return [diffs[diff], i]
            # If difference not in diffs dictionary, add the new difference to the dictionary
            else:
                diffs[num] = i


solution = Solution()
print(solution.twoSum(input_array, target))
